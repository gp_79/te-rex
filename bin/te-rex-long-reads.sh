#Enter samples names separated by space without the ".maf" extension.
#For example:
#samples=(sample_1-recomb sample_2-recomb)

samples=()

#Convert the MAF format in a single-line format where all the information relative to a given recombination events are contained within one line.
#Note: This section intersects the files with the RepeatMasker annotation file in BED format to generate an ID unique for every recombination event.

for prefix in "${samples[@]}"
do
	awk 'ORS = /./ ? "\t" : "\n"' $prefix.maf | tr -s " " | sed 's/ /\t/g' | awk '$5=="s"' | cut -f3,4,6,7,8,9,13 | awk -v OFS="\t" 'm~$7{print m;print;getline}{m=$0}' | paste - - | awk '$7==$14' | sed 's/recomb=//g' | sed 's/>chr/\tchr/g' | awk 'BEGIN{OFS="\t"}{print $3,$1,$6,$11,$9,$14,$16}' > $out_dir_prefix$prefix.maf.tmp1
	cut -f1 $prefix.maf.tmp1 | awk -v OFS="\t" '{gsub("+","\t",$1)}1' | awk -v OFS="\t" '{gsub("-","\t",$1)}1' | awk -v OFS="\t" '{gsub(">","\t",$2)}1' | cut -f1,2 > $out_dir_prefix$prefix".maf.tmp2"
	cut -f4 $prefix.maf.tmp1 | awk -v OFS="\t" '{gsub("+","\t",$1)}1' | awk -v OFS="\t" '{gsub("-","\t",$1)}1' | awk -v OFS="\t" '{gsub(">","\t",$2)}1' | cut -f1,2 > $out_dir_prefix$prefix".maf.tmp3"
	paste $prefix.maf.tmp1 $prefix.maf.tmp2 $prefix.maf.tmp3 | awk 'BEGIN{OFS="\t"}{print $8,$9,$9,$1,$2,$3,$10,$11,$11,$4,$5,$6,$7}' | bedtools intersect -wo -a stdin -b /home/bin/repeatmasker.mod.bed | awk 'BEGIN{OFS="\t"}{print $7,$8,$9,$10,$11,$12,$1,$2,$3,$4,$5,$6,$13,$14"|"$15"|"$16"|"$17"|"$18}' | bedtools intersect -wo -a stdin -b /home/bin/repeatmasker.mod.bed | awk 'BEGIN{OFS="\t"}{print $7,$8,$9,$10,$11,$12,$1,$2,$3,$4,$5,$6,$13,$14,$15"|"$16"|"$17"|"$18"|"$19}' | awk '$14!=$15' | awk 'BEGIN{OFS="\t"}{class=""; dist=($2+$3-$8-$9)/2.0; if($1!=$7) {class="Inter"} else if(-100000<dist && dist<100000) {class="<100Kb"} else {class=">100Kb"} print $0,class}' > $out_dir_prefix$prefix.maf.tmp4

awk 'ORS = /./ ? "\t" : "\n"' $prefix.maf | awk '$5=="s" {print $13, $16=="+" ? $7 "<" $7+$8 : $7+$8 ">" $7}' OFS="\t" | paste - - | awk '$1==$3' | cut -f1,2,4 > $out_dir_prefix$prefix.maf.dir

#This section ranks and reorders the unique ID so that recombination events A-B and B-A are always listed as A-B and counted correctly as 2x A-B. The script also adds the information about the breakpoints usage for each repeat according to Dfam model sequences

cut -f14,15 $prefix.maf.tmp4 | sed 's/\tchr/_chr/g' | awk 'BEGIN {FS = OFS = "_"}{split($1, a, "\\|"); split($2, b, "\\|"); if (a[1] == b[1]) {if (a[2] < b[2]) print $1, $2; else print $2, $1} else {sub(/^[^0-9]+/, "", a[1]); sub(/^[^0-9]+/, "", b[1]); if (a[1]+0 < b[1]+0) print $1, $2; else print $2, $1}}' > $out_dir_prefix$prefix.maf.tmp5
paste $prefix.maf.tmp4 $prefix.maf.tmp5 | cut -f1-13,16,17 > $out_dir_prefix$prefix.maf.mod
cut -f4 $prefix.maf.mod | tr '>-+|' '\t\t\t\t' | cut -f4,6 | awk '{print $1"_"$2}' | awk 'BEGIN {FS = OFS = "_"}{split($1, a, "\\|"); split($2, b, "\\|"); if (a[1] == b[1]) {if (a[2] < b[2]) print $1, $2; else print $2, $1} else {sub(/^[^0-9]+/, "", a[1]); sub(/^[^0-9]+/, "", b[1]); if (a[1]+0 < b[1]+0) print $1, $2; else print $2, $1}}' > $out_dir_prefix$prefix.maf.mod.bp

#This section generates the final unique ID, adds an additional field with the sample name and filters split reads having a mismap score higher than 1e-04. This parameter can be modified to increase or decrease the stringency of the analyses. The scripts also adds the respective orientation information for the split reads

paste $prefix.maf.mod $prefix.maf.mod.bp | awk 'BEGIN{OFS="\t"}{print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15"_"$16}' | awk -vidx=$prefix '{ print $0"\t"idx; }' | sed 's/-recomb//g' | sed 's/mismap=/mismap=\t/g' | awk '$6<1e-04 && $13<1e-04' | sed 's/mismap=\t/mismap=/g' | awk 'BEGIN{OFS="\t"} NR==FNR {a[$1]=$2"\t"$3; next} {print $0, (($13) in a?a[$13]:"n/a")}' $prefix.maf.dir - > $out_dir_prefix$prefix.maf.mod.id

#This section adds an additional field indicating the type of structural variant ("SV") caused by the recombination. Values can be "inv" (inversion), "del" (deletion), "dupl" (duplication) and "Inter" (Interchromosomal)

cat $prefix.maf.mod.id | grep Inter | awk 'BEGIN{OFS="\t"}{print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,"Inter"}' > $out_dir_prefix$prefix.maf.mod.id.Inter

cat $prefix.maf.mod.id | grep -v Inter | cut -f13,15 | sed 's/|/\t/g' | awk '$5!=$9' | awk -F"\t" 'FNR==NR {a[$1];next};($13 in a)' - $prefix.maf.mod.id | awk 'BEGIN{OFS="\t"}{print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,"inv"}' > $out_dir_prefix$prefix.maf.mod.id.inv

cat $prefix.maf.mod.id | grep -v Inter | cut -f13,15,17,18 | sed 's/_/\t/g' | cut -f1,2,3,6,7 | sed 's/|/\t/g' | awk '$5==$10' | cut -f1,12,13 | sed 's/</\t<\t/g' | sed 's/>/\t>\t/g' | awk '($3=="<" && $4<$5) || ($3==">" && $4>$5)' | awk -F"\t" 'FNR==NR {a[$1];next};($13 in a)' - $prefix.maf.mod.id | awk 'BEGIN{OFS="\t"}{print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,"del"}' > $out_dir_prefix$prefix.maf.mod.id.del

cat $prefix.maf.mod.id | grep -v Inter | cut -f13,15,17,18 | sed 's/_/\t/g' | cut -f1,2,3,6,7 | sed 's/|/\t/g' | awk '$5==$10' | cut -f1,12,13 | sed 's/</\t<\t/g' | sed 's/>/\t>\t/g' | awk '($3=="<" && $4>$5) || ($3==">" && $4<$5)' | awk -F"\t" 'FNR==NR {a[$1];next};($13 in a)' - $prefix.maf.mod.id | awk 'BEGIN{OFS="\t"}{print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,"dupl"}' > $out_dir_prefix$prefix.maf.mod.id.dupl

cat $prefix.maf.mod.id.Inter $prefix.maf.mod.id.inv $prefix.maf.mod.id.del $prefix.maf.mod.id.dupl > $out_dir_prefix$prefix.all

#This section generates a list of files containing unique entries for the unique ID for each sample, and generates a list and count of unique IDs of recombination events found in 2 or more samples (redundant recombination events)

cut -f15 $prefix.all | sort | uniq > $out_dir_prefix$prefix.all.cut
done
sort *cut | uniq -c | sort -nr | awk -v OFS="\t" '$1=$1' | awk '$1>1' > recomb-redundant-events

#This section filters out redundant recombination events from each sample and generates files containing only putative somatic recombination events found only in single samples

for prefix in "${samples[@]}"
do
awk -F"\t" 'FNR==NR {a[$2];next};!($15 in a)' recomb-redundant-events $prefix.all > $out_dir_prefix$prefix.uniq
done

#This section removes intermediate and temporary files, leaving only the original .maf files and the post-processed output files

#rm *cut
#rm *dir
#rm *.mod.*
#rm *tmp*

#Expected output: After running the script the folder should contain the initial .maf files, the executable script (my_script.sh), the “redundant-recomb-events” file and the output files with the extension ".all" (including all recombination events) and “.uniq” (only putative somatic events)

#The ".all" and ".uniq"  output files should have the following structure:

#The output files should have the following structure:

#Field 1: chromosome name of split read A
#Field 2: Start position of breakpoint for split read A
#Field 3: End position of breakpoint for split read A
#Field 4: TE-reX Recomb ID for split read A
#Field 5: mismap score for split read A
#Field 6: length of mapped split read A
#Field 7: chromosome name of split read B
#Field 8: Start position of breakpoint for split read B
#Field 9: End position of breakpoint for split read B
#Field 10: TE-reX Recomb ID for split read B
#Field 11: mismap score for split read B
#Field 12: length of mapped split read B
#Field 13: Fastq ID
#Field 14: Genomic distance of recombined repeats (“<100Kb”, “>100Kb”, “Inter”)
#Field 15: Unique ID (Repeat A_Repeat B_Breakpoint A_Breakpoint B)
#Field 16: Sample name
#Field 17: Type of SV generated by the recombination (“inv”, “del”, “dupl”, “Inter”
