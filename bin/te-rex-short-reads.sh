#Enter samples names separated by space without the ".maf" extension.
#For example:
#samples=(sample_1-recomb sample_2-recomb)

samples=()

#The original TE-reX Recomb output is in MAF format, which is divided over 4 lines for a single split read entry. This section converts the MAF format in a single-line format in which all information relative to a given recombination event are contained in one line.
 
for prefix in "${samples[@]}"
do
awk 'ORS = /./ ? "\t" : "\n"' $prefix.maf | awk 'NF==2' RS= FS="\n" | awk '$5=="s" {print $13,$6"_"$7"_"$7+$8"_"$8,$9,$3,$11}' OFS="\t" > $out_dir_prefix$prefix.maf.tmp1
awk 'ORS = /./ ? "\t" : "\n"' $prefix.maf | awk 'NF==2' RS= FS="\n" | awk '$5=="s" {print $4}' | sed 's/>chr/>\tchr/g' | cut -f2 | sed 's/|/\t/g' | sed 's/-/\t/g' | sed 's/+/\t/g' | sed 's/>/\t/g' | cut -f1,2 > $out_dir_prefix$prefix.maf.tmp2
awk 'ORS = /./ ? "\t" : "\n"' $prefix.maf | awk 'NF==2' RS= FS="\n" | awk '$5=="s" {print $4}' | sed 's/>chr/>\tchr/g' | cut -f2 | sed 's/|/\t/g' | sed 's/_//g' > $out_dir_prefix$prefix.maf.tmp3

awk 'ORS = /./ ? "\t" : "\n"' $prefix.maf | awk 'NF==2' RS= FS="\n" | awk '$5=="s" {print $16=="+" ? $7 "<" $7+$8 : $7+$8 ">" $7}' > $out_dir_prefix$prefix.maf.tmp4

paste $out_dir_prefix$prefix.maf.tmp1 $out_dir_prefix$prefix.maf.tmp2 $out_dir_prefix$prefix.maf.tmp3 $out_dir_prefix$prefix.maf.tmp4 | paste - - > $out_dir_prefix$prefix.maf.tmp5.left.right.tmp

cut -f1,10,20 $prefix.maf.tmp5.left.right.tmp > $out_dir_prefix$prefix.maf.dir

cut -f1-9,11-19 $prefix.maf.tmp5.left.right.tmp > $out_dir_prefix$prefix.maf.tmp5.left.right

#This section collapses PCR duplicates (entries sharing the exact same split read sequence) in a single entry. The script additionally filters out split reads with a mismap score higher than 1e-04. This can be modified to increase or decrease the stringency of the downstream analyses.

awk '!seen[$5"_"$14]++ {print $6,$7,$7,$3,$4,$8,$2,$9,$15,$16,$16,$12,$13,$17,$11,$18,$5,$14,$10}' OFS="\t" $prefix.maf.tmp5.left.right | sed 's/mismap=/mismap=\t/g' | awk '$6<1e-04 && $15<1e-04' | sed 's/mismap=\t/mismap=/g' > $out_dir_prefix$prefix.maf.tmp5.left.right.tmp1

#This section filters out Alu and L1 repeat subfamilies not included in downstream analyses

cat $prefix.maf.tmp5.left.right.tmp1 | grep L1 | grep -v L1PREC | grep -v L1M | grep -v L1PB > $out_dir_prefix$prefix.maf.tmp5.left.right.tmp1.L1
cat $prefix.maf.tmp5.left.right.tmp1 | sed 's/>/\t/g' | awk '$7 ~ /Alu[YSJ]/ && $8 ~ /Alu[YSJ]/' | awk 'BEGIN{OFS="\t"}{print $1,$2,$3,$4,$5,$6">"$7">"$8,$9,$10,$11,$12,$13,$14,$15,$16">"$17">"$18,$19,$20,$21,$22,$23}' > $out_dir_prefix$prefix.maf.tmp5.left.right.tmp1.ALU
cat $prefix.maf.tmp5.left.right.tmp1.L1 $prefix.maf.tmp5.left.right.tmp1.ALU > $out_dir_prefix$prefix.ALU-and-L1.tmp1

#This section intersects the files with the RepeatMasker annotation file in BED format to generate an ID unique for every recombination event. The RepeatMasker file in the example below is in the /home/bin directory (highlighted in light blue), please modify the directory name accordingly.
#Note: The original RepeatMasker file has been modified to remove the underscore symbol ("_") from the name of certain repeats to avoid redundancy issues in downstream analyses. The structure of the modified RepeatMasker file remains true to the original file in tab-separated format, that is: chromosome name / start / end / strand / repeat name / repeat type / repeat subfamily (for example: chr1/t67108753/t67109046/t+/tL1P5/tLINE/tL1)

cat $prefix.ALU-and-L1.tmp1 | bedtools intersect -wo -a stdin -b /home/bin/repeatmasker.mod.bed | awk 'BEGIN{OFS="\t"}{print $9,$10,$11,$12,$13,$14,$15,$16,$1,$2,$3,$4,$5,$6,$7,$8,$17,$18,$19,$20"|"$21"|"$22"|"$23"|"$24}' | bedtools intersect -wo -a stdin -b /home/bin/repeatmasker.mod.bed | awk 'BEGIN{OFS="\t"}{print $9,$10,$11,$12,$13,$14,$15,$16,$1,$2,$3,$4,$5,$6,$7,$8,$17,$18,$19,$20,$21"|"$22"|"$23"|"$24"|"$25}' | awk '$21!=$22' | awk 'BEGIN{OFS="\t"}{print $1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20"_"$21}' > $out_dir_prefix$prefix.ALU-and-L1.tmp2

#This section reorders the RepeatMasker ID and the Dfam breakpoints information to ensure that for the order is always the same for independent entries relative to the same recombination event, so that for example "A-B" and "B-A" are not counted as 2 different recombination events but as "2x A-B". The script to each "unique ID" the information relative to the breakpoint used for a given recombination event, according to the Dfam repeat models. The script also collapses and count independent occurrences of the same recombination event and adds genomic distance information for each event ("<100Kb", ">100Kb", "Inter")
	
cut -f20 $prefix.ALU-and-L1.tmp2 | awk 'BEGIN {FS = OFS = "_"}{split($1, a, "\\|"); split($2, b, "\\|"); if (a[1] == b[1]) {if (a[2] < b[2]) print $1, $2; else print $2, $1} else {sub(/^[^0-9]+/, "", a[1]); sub(/^[^0-9]+/, "", b[1]); if (a[1]+0 < b[1]+0) print $1, $2; else print $2, $1}}' > $out_dir_prefix$prefix.ALU-and-L1.tmp3
cut -f6 $prefix.ALU-and-L1.tmp2 | tr '>-+' '   ' | awk '{print $4"_"$6}' | awk 'BEGIN {FS = OFS = "_"}{split($1, a, "\\|"); split($2, b, "\\|"); if (a[1] == b[1]) {if (a[2] < b[2]) print $1, $2; else print $2, $1} else {sub(/^[^0-9]+/, "", a[1]); sub(/^[^0-9]+/, "", b[1]); if (a[1]+0 < b[1]+0) print $1, $2; else print $2, $1}}' > $out_dir_prefix$prefix.ALU-and-L1.tmp4
paste $prefix.ALU-and-L1.tmp2 $prefix.ALU-and-L1.tmp3 | cut -f1-19,21 | paste - $prefix.ALU-and-L1.tmp4 | awk 'BEGIN{OFS="\t"}{print $20"_"$21,$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20"_"$21}' | awk 'BEGIN{OFS="\t"}{print $0,++a[$1]}' | sort -nr -k22 | awk -F"\t" '!seen[$1]++' | cut -f2-22 | awk 'BEGIN{OFS="\t"}{class=""; dist=($2+$3-$10-$11)/2.0; if($1!=$9) {class="Inter"} else if(-100000<dist && dist<100000) {class="<100Kb"} else {class=">100Kb"} print $0,class}' > $out_dir_prefix$prefix.ALU-and-L1.tmp5

#This section adds to each file one field containing the name of the sample, for downstream reference and/or parsing, and the direction of the split reads:

awk -vidx=$prefix '{ print $0"\t"idx; }' $prefix.ALU-and-L1.tmp5 | sed 's/-recomb//g' | awk 'BEGIN{OFS="\t"} NR==FNR {a[$1]=$2"\t"$3; next} {print $0, (($19) in a?a[$19]:"n/a")}' $prefix.maf.dir - > $out_dir_prefix$prefix.ALU-and-L1

#This section adds an additional field indicating the type of structural variant ("SV") caused by the recombination. Values can be "inv" (inversion), "del" (deletion), "dupl" (duplication) and "Inter" (Inter-chromosomal)

cat $prefix.ALU-and-L1 | grep Inter | awk 'BEGIN{OFS="\t"}{print $1,$2,$3,$4,$5,$6,$7,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,"Inter"}' > $out_dir_prefix$prefix.ALU-and-L1.Inter

cat $prefix.ALU-and-L1 | grep -v Inter | cut -f19,20,24,25 | sed 's/_/\t/g' | cut -f1,2,3,6,7 | sed 's/|/\t/g' | awk '$5!=$10' | awk -F"\t" 'FNR==NR {a[$1];next};($19 in a)' - $prefix.ALU-and-L1 | awk 'BEGIN{OFS="\t"}{print $1,$2,$3,$4,$5,$6,$7,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,"inv"}' > $out_dir_prefix$prefix.ALU-and-L1.inv

cat $prefix.ALU-and-L1 | grep -v Inter | cut -f19,20,24,25 | sed 's/_/\t/g' | cut -f1,2,3,6,7 | sed 's/|/\t/g' | awk '$5==$10' | cut -f1,12,13 | sed 's/</\t<\t/g' | sed 's/>/\t>\t/g' | awk '($3=="<" && $4<$5) || ($3==">" && $4>$5)' | awk -F"\t" 'FNR==NR {a[$1];next};($19 in a)' - $prefix.ALU-and-L1 | awk 'BEGIN{OFS="\t"}{print $1,$2,$3,$4,$5,$6,$7,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,"del"}' > $out_dir_prefix$prefix.ALU-and-L1.del

cat $prefix.ALU-and-L1 | grep -v Inter | cut -f19,20,24,25 | sed 's/_/\t/g' | cut -f1,2,3,6,7 | sed 's/|/\t/g' | awk '$5==$10' | cut -f1,12,13 | sed 's/</\t<\t/g' | sed 's/>/\t>\t/g' | awk '($3=="<" && $4>$5) || ($3==">" && $4<$5)' | awk -F"\t" 'FNR==NR {a[$1];next};($19 in a)' - $prefix.ALU-and-L1 | awk 'BEGIN{OFS="\t"}{print $1,$2,$3,$4,$5,$6,$7,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,"del"}' > $out_dir_prefix$prefix.ALU-and-L1.dupl

cat $prefix.ALU-and-L1.Inter $prefix.ALU-and-L1.inv $prefix.ALU-and-L1.del $prefix.ALU-and-L1.dupl > $out_dir_prefix$prefix.all

#4This section generates additional files in which each unique ID relative to a given recombination event is listed only once in individual files. This are then used to generate a list of recombination events found in n≥2 samples, featuring also the number of occurrences
	
cut -f20 $prefix.all | sort | uniq > $out_dir_prefix$prefix.all.cut
done
sort *cut | uniq -c | sort -nr | awk -v OFS="\t" '$1=$1' | awk '$1>1' > redundant-recomb-events

#This section subtracts from each file the recombination events that occur in 2 or more samples of the dataset, generating new files that contain putative somatic events detected only in single samples

for prefix in "${samples[@]}"
do
awk -F"\t" 'FNR==NR {a[$2];next};!($19 in a)' redundant-recomb-events $prefix.all | awk -F"\t" '!seen[$19]++' > $out_dir_prefix$prefix.uniq
done

#This section removes intermediate and temporary files

rm *cut
rm *-L1*
rm *dir
rm *tmp*

#Expected output: After running the script the folder should contain the initial .maf files, the executable script (my_script.sh), the “redundant-recomb-events” file and the output files with the extension ".all" (including all recombination events) and “.uniq” (only putative somatic events)

#The ".all" and ".uniq"  output files should have the following structure:

#Field $1: chromosome name of split read A
#Field $2: Start position of breakpoint for split read A
#Field $3: End position of breakpoint for split read A
#Field $4: Strand for split read A
#Field $5: mismap score for split read A
#Field $6: TE-reX Recomb ID for split read A
#Field $7: mapping information for split read A (genomic location and length)
#Field $8: chromosome name of split read B
#Field $9: Start position of breakpoint for split read B
#Field $10: End position of breakpoint for split read B
#Field $11: Strand for split read B
#Field $12: mismap score for split read B
#Field $13: TE-reX Recomb ID for split read B
#Field $14: mapping information for split read B (genomic location and length)
#Field $15: identity of the recombined repeats according to Dfam models
#Field $16: Sequence of split read A
#Field $17: Sequence of split read B
#Field $18: Fastq ID
#Field $19: Unique ID (Repeat A_Repeat B_Breakpoint A_Breakpoint B)
#Field $20: Independent supporting reads count
#Field $21: Genomic distance of recombined repeats (“<100Kb”, “>100Kb”, “Inter”)
#Field $22: Sample name
#Field $23: Type of SV generated by the recombination (“inv”, “del”, “dupl”, “Inter”)
