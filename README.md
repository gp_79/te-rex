# TE-reX

TE-reX finds recombinations involving transposable elements (or any
interspersed repeats), in DNA sequences.  It can do 2 things:

1.  Find repeat elements, in DNA (e.g. a genome), that are hybrids of
    known repeat types.  For example, half `LTR8A` and half `LTR8B`.
    In other words: two adjacent parts of the element match two
    different repeat types, and the boundary occurs at homologous
    coordinates of those repeat types.  Such hybrids may be:

    * Products of homologous recombination between similar repeats.
    * Unknown repeat types, similar to some known types (e.g. if the
      closest known type has a deletion).

    Details [here](genome.md).

2.  Find recombinations involving repeats, in (short or long) DNA
    reads relative to a reference genome.  It finds two kinds of
    recombination:

    - Homologous recombination
    - Transposition/retrotransposition candidates

    Details [here](reads.md).

TE-reX depends on [LAST][], especially [last-split][], which finds
the most probable division of a query sequence into parts together
with the most probable alignment of each part to reference sequences.

For more information, please see: [Recombination of repeat elements
generates somatic complexity in human
genomes](https://doi.org/10.1016/j.cell.2022.06.032), Cell 2022
185(16):3025-3040.e6.

[LAST]: https://gitlab.com/mcfrith/last
[last-split]: https://gitlab.com/mcfrith/last/-/blob/main/doc/last-split.rst
