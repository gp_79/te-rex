# TE-reX for finding hybrid repeat elements in a genome

The workflow is to align our genome to repeat consensus sequences,
then seek hybrid elements.

## Getting repeat consensus sequences

We can download consensus sequences of (say) human repeats (where
`9606` is the taxonomy ID for *Homo sapiens*), from [Dfam][]:

    dfam-fasta -c9606 -x'root;Interspersed_Repeat' > reps.fa

It's recommended to get all interspersed repeats, like this, even if
you're only interested in a subset.  For example, if you only get
[SVA][] elements, they will misleadingly match Alus in the genome.

`dfam-fasta --help` shows its options: they are explained in the [Dfam
API "families"
documentation](https://dfam.org/releases/Dfam_3.0/apidocs/#tag-families).

## Genome-to-repeat alignments

We can align each part of the genome to the most-similar consensus
sequence, using [LAST][].  *This recipe assumes LAST version >= 1387*!!
First, make an index (called, say, "repdb") of the consensus
sequences:

    lastdb repdb reps.fa

Next, [determine the rates of insertion, deletion and
substitutions][train] between genome and consensus sequences:

    last-train -P8 --revsym -X1 -m100 repdb genome.fa > rep.mat

* `-P8` makes it faster by using 8 parallel threads: adjust as
  suitable for your computer.  This has no effect on the results.
* `-X1` tells it to treats `N`s in the consensus sequences as unknown
  bases.
* `-m100` makes it more slow-and-sensitive than the default.

Then, find alignments:

    lastal -P8 -p rep.mat -m100 -D1e7 --split repdb genome.fa | last-postmask > genome-to-reps.maf

* `-P8` makes it use 8 parallel threads: modify this as you wish.
  This has no effect on the results.

(This alignment method is not ideal, because it trains one set of
substitution and gap rates for all repeat classes, whereas these rates
differ for younger and older classes.)

## Repeat-to-repeat alignments

`TE-reX` needs to know which parts of the repeat consensus sequences
are homologous to each other.  We can align them to each other like
this:

    te-self-align hg38-calJac3.mat reps.fa > reps-to-reps.tab

`hg38-calJac3.mat` has substitution and gap rates for human versus
marmoset DNA, which seems to work OK for human Alu-Alu and L1-L1
alignments.  It was made like this:

    last-train -P8 --revsym --matsym --gapsym -E0.05 -C2 gdb calJac3.fa

## Finding hybrid elements

Finally!  We can run `te-rex`:

    te-rex genome-to-reps.maf reps-to-reps.tab my-out

This makes two output files: `my-out.bed` and `my-out.txt`.  The
former is in [BED][] format:

    chr1    4039976  4040901  L1P1_5end#LINE/L1   4.25e-09  +
    chr1    4040901  4042156  L1P2_5end#LINE/L1   1e-10     +

    chr1    4114075  4114501  L1MEf_5end#LINE/L1  1e-10     -
    chr1    4114505  4115442  L1MD2_5end#LINE/L1  1e-10     -

    chr1    4492906  4493197  MLT2A2#LTR/ERVL     1e-10     +
    chr1    4493199  4493308  MLT2B3#LTR/ERVL     1e-10     +
    chr1    4493309  4493454  MLT2A2#LTR/ERVL     1.11e-05  +

Each group of lines (separated by blank lines) is one hybrid element,
and each line is one genome-to-consensus match.  The first 3 columns
show the chromosome start and end coordinates, and column 4 is the
consensus name.  Column 5 is the [mismap probability][], i.e. the
probability that this part of the genome should be aligned to a
different consensus.  Column 7 shows whether the consensus matches the
chromosome's `+` or `-` strand.

`my-out.txt` shows the count of each type of hybrid (e.g. `L1P1_5end`
followed by `L1P2_5end`):

    2058  MLT2A2#LTR/ERVL       MLT2B3#LTR/ERVL       MLT2A2#LTR/ERVL
    212   L1MD2_5end#LINE/L1    L1MEf_5end#LINE/L1
    124   L1P1_5end#LINE/L1     L1P2_5end#LINE/L1

Here's a way to view it with nice column formatting:

    column -t my-out.txt | less

## `te-rex` options

- `-h`, `--help`: show a help message, with default option values, and
  exit.

- `-s BP`, `--slop=BP`: maximum distance between adjacent parts of a
  hybrid element, and maximum offset from homologous coordinates of
  the consensus sequences.

`te-rex` has some other options, but they have no effect except when
[analyzing DNA reads](reads.md).

[LAST]: https://gitlab.com/mcfrith/last
[train]: https://gitlab.com/mcfrith/last/-/blob/main/doc/last-train.rst
[mismap probability]: https://gitlab.com/mcfrith/last/-/blob/main/doc/last-split.rst
[BED]: https://genome.ucsc.edu/FAQ/FAQformat.html#format1
[Dfam]: https://dfam.org/home
[SVA]: https://dfam.org/browse?name_accession=SVA
