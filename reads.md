# TE-reX for finding recombinations in DNA reads

The workflow is to align our DNA reads to the genome, then seek
recombinations.

## Aligning "short" (Illumina) DNA reads to the genome

We need to align the reads to the genome carefully, so each part of a
read matches the right repeat.  We can use [LAST][].  *This recipe
assumes LAST version >= 1387*!!  First, we make an index (called
"gdb") of the genome:

    lastdb -P8 -uNEAR gdb genome.fa

* `-P8` makes it use 8 parallel threads: modify this as you wish.
  This has no effect on the results.
* It's OK to use gzipped (`.gz`) files, in this and later steps.

Next, we [determine the rates of insertion, deletion, and
substitutions][train] between reads and genome.  It's probably OK to
do this just once with a representative file of reads:

    last-train -P8 -Q1 gdb reads.fq > train.out

* `-P8` makes it use 8 parallel threads: modify this as you wish.
  This has no effect on the results.

Next, we align the reads to the genome:

    lastal -P8 -p train.out -C2 -D1e9 --split gdb reads.fq | last-postmask > reads.maf

* `-P8` makes it use 8 parallel threads: modify this as you wish.
  This has no effect on the results.
* `-C2` makes it faster, with probably little harm to accuracy.
* `-D1e9` sets a stricter threshold for alignment significance.  Since
  we are looking for rare-and-interesting events, we should be
  especially careful to avoid spurious alignments.

You can compress the output, by modifying the preceding command like
this:

    ... | last-postmask | gzip > reads.maf.gz

## Aligning "long" (nanopore or PacBio) DNA reads to the genome

We can use the same method as above, with 3 changes:

* For `lastdb`, use `-uRY4` instead of `-uNEAR` (reduces time and memory use).
* For `last-train`, use `-Q0` instead of `-Q1`.
* For `lastal`, don't use `-C2`.

## Finding recombinations

    te-rex genome-to-reps.maf reps-to-reps.tab reads.maf my-out

This reads 3 input files:

* `genome-to-reps.maf`: alignments of the reference genome to repeat
  consensus sequences.
* `reps-to-reps.tab`: alignments between repeat consensus sequences
  (needed to identify homologous recombination).
* `reads.maf`: our read-to-genome alignments.

How to get `genome-to-reps.maf` and `reps-to-reps.tab` is described
[here](genome.md).

`te-rex` creates 2 files whose names start with "my-out":

### `my-out-recomb.maf`

This has the alignments of reads that indicate homologous
recombination, with information like this added:

    myDnaRead+76>chr21-46510734>AluY+195>AluSq2+196|237/284

This indicates homologous recombination between an `AluY` and an
`AluSq2`.  It means the alignment of `myDnaRead` has a breakpoint at
coordinate 76, which aligns to the `-` strand of `chr21` coordinate
46510734, which aligns to the `+` strand of `AluY` coordinate 195,
which aligns to the `+` strand of `AluSq2` coordinate 196.  All
coordinates are [in-between
coordinates](https://www.biostars.org/p/84686/).

Homologous recombination is inferred only if a DNA read "jumps"
between near-exactly homologous coordinates of 2 repeat elements.

Finally, `237/284` means that this `AluY` (in chromosome 21) aligns to
this `AluSq2` with 237 matches and 284 matches+mismatches.  (Warning:
in rare cases, this can be `0/0`.  One reason this can happen is that
some Dfam consensus sequences are split into parts for e.g. the 3'-
and 5'-end of a repeat, and `te-rex` isn't clever enough to merge
these parts.)

### `my-out-insert.maf`

This has the alignments of reads that indicate (retro)transposition
candidates, with information like this added:

    AluYa5[0.00132]0-87(224)

This means the alignment overlaps coordinates 0 to 87 of an AluYa5
element.  224 is the number of bases left, beyond 87, in the AluYa5
consensus sequence.  0.00132 is the probability that the element is
wrongly classified (i.e. it's a different kind of Alu).

In simple cases, the layout is added, e.g.

* `here|AluYa5>` means that an AluYa5 has inserted, in forward
  orientation, immediately after the reference-genome sequence shown
  in this alignment.

* `<AluYc|here` means that an AluYc has inserted, in reverse
  orientation, immediately before the reference-genome sequence shown
  in this alignment.

#### Is it really (retro)transposition?

It seems hard to distinguish (retro)transposition from other kinds of
rearrangement.  Here are some things to check:

* Is it a young, highly-active type of transposable element
  (e.g. L1HS, AluYa5, AluYb8)?  Then it's more likely to be real.

* Is it actually homologous recombination between two similar repeats?
  `te-rex` infers homologous recombination strictly, and may miss
  some.  Does the DNA read "jump" between almost-identical coordinates
  of similar repeats?

* Does the DNA read "jump" between nearby places in one chromosome?
  If so, maybe it's a small inversion or tandem duplication that
  happens to cut a repeat.

* Does the rearrangement happen near an end of the repeat consensus
  sequence?  Although truncated retrotransposition occurs, if the
  rearrangement happens right at the edge of a repeat consensus, it
  would be quite a coincidence if it were not (retro)transposition.

* Is there an insertion-site motif?

#### L1 endonuclease motif

In humans, retrotransposition often occurs at sites cleaved by L1
endonuclease, which cleaves at the postion marked `|` in sequences
similar to `5'-TTTT|AA-3'` ([Flasch et al. 2019][]).  The other DNA
strand tends to cleave ~15 bp downstream ([Kojima 2010][]), like this:

    5' ooooooooTTAAAAoooooooooooooooooo 3'
    3' ooooooooAATTTToooooooooooooooooo 5'


    5' ooooooooTTAAAAooooooooooo                ooooooo 3'
    3' ooooooooAA                TTTToooooooooooooooooo 5'


    5' ooooooooTTAAAAooooooooooo|repeat>AAAAdddddddddddooooooo 3'
    3' ooooooooAATTTTddddddddddd|repeat>TTTToooooooooooooooooo 5'

As a result:

* For `here|repeat>`, we expect to see TTAAAA (or similar) ~15 bp
  before the end of the genomic sequence in this alignment.

* For `<repeat|here`, we expect to see TTTTAA (or similar) ~15 bp
  after the start of the genomic sequence in this alignment.

## Tricks for parsing the `te-rex` output files

The `te-rex` output files have multi-line alignments, with single
blank lines separating the alignments of one read, and double blank
lines separating reads.  It may be useful to put each alignment on one
line, with single blank lines separating reads:

    awk 'ORS = /./ ? "\t" : "\n"' in.maf > out-file

(This means: end non-blank lines with a tab, and blank lines with a
newline.)  Another trick is to make `awk` read a paragraph at a time,
instead of a line at a time, with `RS=`:

    awk '{print $7, $8, $whatever}' RS= in-file > out-file

## `te-rex` options

- `-h`, `--help`: show a help message, with default option values, and
  exit.

- `-m PROB`, `--mismap=PROB`: maximum mismap probability.

    * For homologous recombination: of the 2 alignments indicating a
      recombination, at least 1 must have mismap probability <= PROB.
    * For insertions: the alignment of the target site must have
      mismap probability <= PROB.

- `-g BP`, `--min-gap=BP`: don't infer (retro)transposition when it
  could be explained as a deletion of length < BP.

- `-s BP`, `--slop=BP`: infer homologous recombination if a DNA read
  "jumps" between homologous coordinates of 2 repeat elements,
  allowing up to this many bp offset from exactly homologous
  coordinates.

## FAQ

**Q:** Why not use RepeatMasker?  
**A:** We need precise base-level alignments between the genome and
repeats.  RepeatMasker provides this in `.align` files, but they seem
to be riddled with errors.

**Q:** What about paired reads?  
**A:** `te-rex` does not (currently) consider pair relationships
between reads.

[LAST]: https://gitlab.com/mcfrith/last
[train]: https://gitlab.com/mcfrith/last/-/blob/main/doc/last-train.rst
[SVA]: https://dfam.org/browse?name_accession=SVA
[Flasch et al. 2019]: https://www.ncbi.nlm.nih.gov/pubmed/30955886
[Kojima 2010]: https://www.ncbi.nlm.nih.gov/pubmed/20615209
